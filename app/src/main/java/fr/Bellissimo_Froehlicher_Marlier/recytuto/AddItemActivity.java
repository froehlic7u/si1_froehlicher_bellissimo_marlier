package fr.Bellissimo_Froehlicher_Marlier.recytuto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static fr.Bellissimo_Froehlicher_Marlier.recytuto.R.id.recycler;

public class AddItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.additem);

    }

    public boolean addTodoItem(View v){
        EditText editLabel = (EditText) findViewById(R.id.editText2);
        String label = String.valueOf(editLabel.getText());
        Spinner spinnerTag = (Spinner)findViewById(R.id.spinner5);
        TodoItem.Tags tag = null;
        DatePicker dateEcheance = (DatePicker)findViewById(R.id.datePicker4);

        if(label.length()==0){
            Toast.makeText(this,"Vous devez rentrez un label",Toast.LENGTH_SHORT).show();
            return false;
        }

        Calendar date = new GregorianCalendar(dateEcheance.getYear(), dateEcheance.getMonth(), dateEcheance.getDayOfMonth());

        switch (String.valueOf(spinnerTag.getSelectedItem())){
            case "Faible":
                tag = TodoItem.Tags.Faible;
                break;
            case "Normal":
                tag = TodoItem.Tags.Normal;
                break;
            case "Important":
                tag = TodoItem.Tags.Important;
                break;
            default:
                Toast.makeText(this,"Le tag n'est pas valide",Toast.LENGTH_SHORT).show();
                return false;
        }

        TodoItem newItem = new TodoItem(tag,label, date);
        Toast.makeText(this,"Item ajouté avec succès",Toast.LENGTH_SHORT).show();
        TodoDbHelper.addItem(newItem, getBaseContext());
        startActivity(new Intent(AddItemActivity.this,MainActivity.class));
        finish();
        return true;
    }

    public void returnHome(View v){
        startActivity(new Intent(AddItemActivity.this,MainActivity.class));
        finish();
    }

}
