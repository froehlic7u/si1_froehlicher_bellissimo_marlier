package fr.Bellissimo_Froehlicher_Marlier.recytuto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by phil on 06/02/17.
 */

public class TodoItem implements Serializable{

    public enum Tags {
        Faible("Faible"), Normal("Normal"), Important("Important");

        private String desc;
        Tags(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    private long id;
    private String label;
    private Tags tag;
    private boolean done;
    private int ordre;
    private Calendar echeance;

    public TodoItem(Tags tag, String label, Calendar echeance) {
        this.tag = tag;
        this.label = label;
        this.done = false;
        this.ordre = 0;
        this.echeance = echeance;
    }

    public TodoItem(String label, Tags tag, Calendar echeance, boolean done) {
        this.label = label;
        this.tag = tag;
        this.done = done;
        this.ordre = 0;
        this.echeance = echeance;
    }

    public static Tags getTagFor(String desc) {
        for (Tags tag : Tags.values()) {
            if (desc.compareTo(tag.getDesc()) == 0)
                return tag;
        }

        return Tags.Faible;
    }

    public String getLabel() {
        return label;
    }

    public Tags getTag() {
        return tag;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setTag(Tags tag) {
        this.tag = tag;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setOrdre(int o) { ordre = o; }

    public int getOrdre() { return ordre ;}

    public void setId(long i) { id = i; }

    public long getId() { return id; }

    public Calendar getEcheanceDate() { return echeance; }

    public String getEcheanceAffichage() {
        int jour = echeance.get(Calendar.DAY_OF_MONTH);
        int mois = echeance.get(Calendar.MONTH)+1;
        int annee = echeance.get(Calendar.YEAR);
        return ("Echéance : "+jour+"/"+mois+"/"+annee);
    }

    public void setEcheance(Calendar echeance) { this.echeance = echeance; }
}
