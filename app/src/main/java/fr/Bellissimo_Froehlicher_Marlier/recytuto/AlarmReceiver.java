package fr.Bellissimo_Froehlicher_Marlier.recytuto;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class AlarmReceiver extends BroadcastReceiver {

    public static final int ID_NOTIFICATION = 18125;
    @Override
    public void onReceive(Context context, Intent intent) {
        int nb=nbElements(context);
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle("TodoItem")
                        .setContentText("Il reste "+nb+" tâches à accomplir");
        Intent resultIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(14551, mBuilder.build());

    }

    public int nbElements(Context context){
        ArrayList<TodoItem> items=TodoDbHelper.getItems(context);
        int nb=0;
        for(TodoItem ti : items){
            if(!ti.isDone()&& ti.getEcheanceDate().after(GregorianCalendar.getInstance()))
                nb++;
        }
        return nb;
    }
}