package fr.Bellissimo_Froehlicher_Marlier.recytuto;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created by phil on 07/02/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoHolder> {

    protected ArrayList<TodoItem> items;

    public RecyclerAdapter(ArrayList<TodoItem> items) {
        this.items = items;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new TodoHolder(inflatedView, items);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        TodoItem it = items.get(position);
        holder.bindTodo(it);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class TodoHolder extends RecyclerView.ViewHolder {
        private Resources resources;
        private ImageView image;
        private Switch sw;
        private TextView label;
        private TextView echeance;
        private ArrayList<TodoItem> listItems;

        public TodoHolder(final View itemView, ArrayList<TodoItem> items) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageView);
            sw = (Switch) itemView.findViewById(R.id.switch1);
            label = (TextView) itemView.findViewById(R.id.textView);
            echeance = (TextView) itemView.findViewById(R.id.textView2);
            resources = itemView.getResources();
            listItems = items;


            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v)
                {
                    //Toast.makeText(v.getContext(), "Position is " + getAdapterPosition(), Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

                    builder.setMessage("Voulez vous supprimer la tache "+label.getText());
                    builder.setTitle(R.string.titre_dialogue);

                    builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            TodoItem it = listItems.get(getAdapterPosition());
                            TodoDbHelper.removeItem(it, v.getContext());
                            ((MainActivity)v.getContext()).reloadElements();
                        }
                    });
                    builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();


                    return false;
                }
            }
            );

            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    TodoItem it = listItems.get(getAdapterPosition());
                    if(it.getEcheanceDate().after(GregorianCalendar.getInstance())) {
                        if (sw.isChecked()) {
                            itemView.setBackgroundColor(Color.LTGRAY);
                            if (!it.isDone()) {
                                it.setDone(true);
                                TodoDbHelper.updateItem(it, buttonView.getContext());
                            }
                        } else {
                            itemView.setBackgroundColor(Color.TRANSPARENT);
                            if (it.isDone()) {
                                it.setDone(false);
                                TodoDbHelper.updateItem(it, buttonView.getContext());
                            }
                        }
                    }
                }
            });
        }

        public void changeBackground(View itemView){
            itemView.setBackgroundColor(Color.GRAY);
        }



        public void bindTodo(TodoItem todo) {
            label.setText(todo.getLabel());
            sw.setChecked(todo.isDone());
            if(todo.getEcheanceDate().before(GregorianCalendar.getInstance())) {
                itemView.setBackgroundColor(Color.argb(20, 247, 0, 51));
                echeance.setText("Cette tâche est arrivée à échéance");
                sw.setEnabled(false);
            }else
                echeance.setText(todo.getEcheanceAffichage());
            switch(todo.getTag()) {
                case Faible:
                    image.setBackgroundColor(resources.getColor(R.color.faible));
                    break;
                case Normal:
                    image.setBackgroundColor(resources.getColor(R.color.normal));
                    break;
                case Important:
                    image.setBackgroundColor(resources.getColor(R.color.important));
                    break;

            }
        }

    }
}
