package fr.Bellissimo_Froehlicher_Marlier.recytuto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Lucas on 22/03/2017.
 */

public class ModifyItemActivity extends AppCompatActivity {

    TextView label;
    Spinner importance;
    DatePicker echeance;
    TodoItem itemCourant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_item);
        itemCourant = (TodoItem)getIntent().getSerializableExtra("ITEM");
        label = (TextView)findViewById(R.id.label);
        importance = (Spinner)findViewById(R.id.importance);
        echeance = (DatePicker)findViewById(R.id.dateEch);
        label.setText(itemCourant.getLabel());
        ArrayAdapter adapter = (ArrayAdapter)importance.getAdapter();
        importance.setSelection(adapter.getPosition(itemCourant.getTag().getDesc()));
        Calendar madate = Calendar.getInstance();
        madate.setTimeInMillis(itemCourant.getEcheanceDate().getTimeInMillis());
        echeance.updateDate(madate.get(Calendar.YEAR), madate.get(Calendar.MONTH), madate.get(Calendar.DAY_OF_MONTH));
    }

    public boolean updateTodoItem(View v){
        String textLabel = label.getText().toString();

        if(textLabel.length()==0){
            Toast.makeText(this,"Vous devez rentrez un label",Toast.LENGTH_SHORT).show();
            return false;
        }

        Calendar date = new GregorianCalendar(echeance.getYear(), echeance.getMonth(), echeance.getDayOfMonth());

        TodoItem.Tags tag;
        switch (String.valueOf(importance.getSelectedItem())){
            case "Faible":
                tag = TodoItem.Tags.Faible;
                break;
            case "Normal":
                tag = TodoItem.Tags.Normal;
                break;
            case "Important":
                tag = TodoItem.Tags.Important;
                break;
            default:
                Toast.makeText(this,"Le tag n'est pas valide",Toast.LENGTH_SHORT).show();
                return false;
        }

        itemCourant.setLabel(textLabel);
        itemCourant.setTag(tag);
        itemCourant.setEcheance(date);
        Toast.makeText(this,"Item modifié avec succès",Toast.LENGTH_SHORT).show();
        TodoDbHelper.updateItem(itemCourant, getBaseContext());
        startActivity(new Intent(ModifyItemActivity.this,MainActivity.class));
        finish();
        return true;
    }

    public void returnHome(View v){
        startActivity(new Intent(ModifyItemActivity.this,MainActivity.class));
        finish();
    }

}
