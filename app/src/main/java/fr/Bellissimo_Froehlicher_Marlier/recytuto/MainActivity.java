package fr.Bellissimo_Froehlicher_Marlier.recytuto;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;


public class MainActivity extends AppCompatActivity {

    private ArrayList<TodoItem> items;
    private RecyclerView recycler;
    private LinearLayoutManager manager;
    private RecyclerAdapter adapter;
    private AlarmService alarmService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addManager = new Intent(MainActivity.this,AddItemActivity.class);
                startActivity(addManager);
                finish();
            }
        });
        Log.i("INIT", "Fin initialisation composantes");



        // Test d'ajout d'un item
        //TodoItem item = new TodoItem(TodoItem.Tags.Important, "Réviser ses cours");
        //TodoDbHelper.addItem(item, getBaseContext());
        //item = new TodoItem(TodoItem.Tags.Normal, "Acheter du pain");
        //TodoDbHelper.addItem(item, getBaseContext());


        Log.i("INIT", "Fin initialisation items");

        // On initialise le RecyclerView
        recycler = (RecyclerView) findViewById(R.id.recycler);
        manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);

        this.reloadElements();

        setRecyclerViewItemTouchListener();
        Log.i("INIT", "Fin initialisation recycler");

        alarmService = new AlarmService(this);
        alarmService.startAlarm();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent dbmanager = new Intent(this,AndroidDatabaseManager.class);
            startActivity(dbmanager);
        }
        else if(id == R.id.vider) {
            TodoDbHelper.viderTable(getBaseContext());
            items = new ArrayList<TodoItem>();
            adapter = new RecyclerAdapter(items);
            recycler.setAdapter(adapter);
            Toast.makeText(this, "La liste a été vidée", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = viewHolder1.getAdapterPosition();
                if(fromPosition < toPosition){
                    for(int i = fromPosition; i<toPosition; i++) {
                        int ordre1 = items.get(i).getOrdre();
                        int ordre2 = items.get(i+1).getOrdre();
                        items.get(i).setOrdre(ordre2);
                        items.get(i+1).setOrdre(ordre1);
                        TodoDbHelper.updateItem(items.get(i), getBaseContext());
                        TodoDbHelper.updateItem(items.get(i+1), getBaseContext());
                        Collections.swap(items, i, i + 1);
                    }
                }
                else {
                    for(int i = fromPosition; i>toPosition; i--) {
                        int ordre1 = items.get(i).getOrdre();
                        int ordre2 = items.get(i-1).getOrdre();
                        items.get(i).setOrdre(ordre2);
                        items.get(i-1).setOrdre(ordre1);
                        TodoDbHelper.updateItem(items.get(i), getBaseContext());
                        TodoDbHelper.updateItem(items.get(i-1), getBaseContext());
                        Collections.swap(items, i, i - 1);
                    }
                }
                adapter.notifyItemMoved(fromPosition, toPosition);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                TodoItem item = items.get(position);

                switch(swipeDir) {
                    case ItemTouchHelper.RIGHT:
                        if(item.isDone())
                            item.setDone(false);
                        else
                            item.setDone(true);
                        TodoDbHelper.updateItem(item, getBaseContext());
                        MainActivity.this.reloadElements();
                        break;
                    case ItemTouchHelper.LEFT:
                        Intent it = new Intent(MainActivity.this, ModifyItemActivity.class);
                        it.putExtra("ITEM", item);
                        startActivityForResult(it, 0);
                        finish();
                        break;
                }
                //cette méthode est sale et fait un clignotement, mais si on fait un notifyItemChanged, des bugs sont visible avec les swipes
                //si on swipe avec une activité à échéance, parfois, la suivant ou précédente change également de couleur au swipe suivant, sans raison.
                //D'où notre choix d'utiliser "reloadElements" pour corriger ces bugs.
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recycler);
    }

    public void reloadElements(){
        // On récupère les items
        items = TodoDbHelper.getItems(getBaseContext());

        // Gestion de l'ordre de priorité, en fonction de l'ordre dans la liste
        for(int i = 0; i < items.size(); i++){
            items.get(i).setOrdre(i+1);
            TodoDbHelper.updateItem(items.get(i), getBaseContext());
        }
        adapter = new RecyclerAdapter(items);
        recycler.setAdapter(adapter);
    }



}
